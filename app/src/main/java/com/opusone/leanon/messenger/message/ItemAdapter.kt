package com.opusone.leanon.messenger.message

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.opusone.leanon.messenger.R
import com.opusone.leanon.messenger.message.OoMessageType.Companion.ITEM_MESSAGE_NORMAL
import com.opusone.leanon.messenger.message.OoMessageType.Companion.ITEM_MESSAGE_NORMAL_SENT
import com.opusone.leanon.messenger.message.OoMessageType.Companion.ITEM_MESSAGE_SELECTED
import com.opusone.leanon.messenger.message.OoMessageType.Companion.ITEM_MESSAGE_SELECTED_SENT
import com.opusone.leanon.messenger.message.OoMessageType.Companion.ITEM_MESSAGE_SELECTOR
import com.opusone.leanon.messenger.message.OoMessageType.Companion.ITEM_MESSAGE_SELECTOR_SENT
import com.opusone.leanon.oocontentresolver.model.User
import com.opusone.leanon.oorealmmanager.model.OoRmMessage
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_datechanged.view.*
import kotlinx.android.synthetic.main.item_guardian_message.view.*
import kotlinx.android.synthetic.main.item_guardian_selector.view.*
import kotlinx.android.synthetic.main.item_senior.view.*
import kotlinx.android.synthetic.main.item_senior_answer.view.*
import java.text.SimpleDateFormat
import java.util.*


class ItemAdapter(var user: User?, var messageList : OrderedRealmCollection<OoRmMessage>) : RealmRecyclerViewAdapter<OoRmMessage, RecyclerView.ViewHolder>(messageList, true) {
    private val TAG = "ItemAdapter"

    var selectorClickedListener: SelectorClickedListener?= null
    private lateinit var selectorAdapter: SelectorAdapter

    private val simpleDateFormat = SimpleDateFormat("yyyy년 M월 d일 E요일", Locale.getDefault())
    private val shortDateFormat = SimpleDateFormat("a h시 mm분", Locale.getDefault())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view : View
        when(viewType) {
            ITEM_MESSAGE_NORMAL -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_guardian_message, parent, false)
                return GuardianViewHolder(view)
            }
            ITEM_MESSAGE_SELECTOR -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_guardian_selector, parent, false)
                return GuardianSelectorViewHolder(view)
            }
            ITEM_MESSAGE_NORMAL_SENT -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_senior, parent, false)
                return SeniorViewHolder(view)
            }
            ITEM_MESSAGE_SELECTED_SENT -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_senior_answer, parent, false)
                return SeniorAnswerViewHolder(view)
            }
            else -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_datechanged, parent, false)
                return DateChangeViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        if (MessagingManager.dataSet == null) {
            return 0
        }
        return messageList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val messageData = messageList[position]

        if (holder is GuardianViewHolder) {
            handleGuardianMessage(holder, messageData)

        } else if (holder is GuardianSelectorViewHolder) {
            handleGuardianSelectorMessage(holder, messageData)

        } else if (holder is SeniorViewHolder) {
            handleSeniorMessage(holder, messageData)

        } else if (holder is DateChangeViewHolder) {
            handleDateChangeMessage(holder, messageData)

        } else if (holder is SeniorAnswerViewHolder) {
            handleSeniorAnswerMessage(holder, messageData)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val senderId = messageList[position].id

        when(messageList[position].dataType) {
            ITEM_MESSAGE_NORMAL -> {
                if(senderId.equals(user?.id)) {
                    return ITEM_MESSAGE_NORMAL_SENT
                } else {
                    return ITEM_MESSAGE_NORMAL
                }
            }
            ITEM_MESSAGE_SELECTOR -> {
                if(senderId.equals(user?.id)) {
                    return ITEM_MESSAGE_SELECTOR_SENT
                } else {
                    return ITEM_MESSAGE_SELECTOR
                }
            }
            ITEM_MESSAGE_SELECTED -> {
                if(senderId.equals(user?.id)) {
                    return ITEM_MESSAGE_SELECTED_SENT
                } else {
                    return ITEM_MESSAGE_SELECTED
                }
            }
            else -> {
                return OoMessageType.ITEM_MESSAGE_DATECHANGED
            }
        }
    }

    private fun handleGuardianMessage(holder: GuardianViewHolder, message: OoRmMessage) {
        holder.guardianName.text = message.name
        holder.guardianMessage.text =message.message
        holder.guardianSendtime.text = shortDateFormat.format(message.timestamp)
        Glide.with(holder.itemView.context)
            .load(message.picture)
            .centerCrop()
            .circleCrop()
            .into(holder.guardianAccount)
    }

    private fun handleGuardianSelectorMessage(holder: GuardianSelectorViewHolder, message: OoRmMessage) {
        holder.guardianSelectorSendTime.text = shortDateFormat.format(message.timestamp)
        holder.guardianSelectorName.text = message.name
        holder.guardianSelectorMessage.text = message.message
        Log.i(TAG, message.messageAnswer.toString())
        selectorAdapter =
            SelectorAdapter(message.messageAnswer)
        selectorAdapter.onSelectorClicked = {
            it?.let {selected ->
                selectorClickedListener?.let {
                    it(selected, message.question, message.message)
                }
            }
        }
        holder.guardianBtnContainer.adapter = selectorAdapter
        Glide.with(holder.itemView.context)
            .load(message.picture)
            .centerCrop()
            .circleCrop()
            .into(holder.guardianSelectorAccount)
    }

    private fun handleSeniorMessage(holder: SeniorViewHolder, message: OoRmMessage) {
        holder.seniorMessage.text = message.message
        holder.seniorSendTime.text = shortDateFormat.format(message.timestamp)
    }

    private fun handleDateChangeMessage(holder: DateChangeViewHolder, message: OoRmMessage) {
        holder.datechangeView.text = simpleDateFormat.format(message.timestamp)
    }

    private fun handleSeniorAnswerMessage(holder: SeniorAnswerViewHolder, message: OoRmMessage) {
        holder.seniorAnswerSendTime.text = shortDateFormat.format(message.timestamp)
        holder.seniorAnswer.text = message.messageAnswer?.get(0)
        holder.seniorQuestion.text = message.message
    }
}

class GuardianViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var guardianName = itemView.guardian_name
    var guardianAccount = itemView.account_image
    var guardianMessage = itemView.messagebox
    var guardianSendtime = itemView.guardian_sendtime
}
class GuardianSelectorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var guardianSelectorAccount = itemView.selector_account_image
    var guardianSelectorName = itemView.selector_guardian_name
    var guardianSelectorMessage = itemView.selector_messagebox
    var guardianBtnContainer = itemView.selector_recyclerview
    var guardianSelectorSendTime = itemView.selector_sendtime
}

class DateChangeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var datechangeView = itemView.datechange_txt
}

class SeniorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var seniorMessage = itemView.messagebox_senior
    var seniorSendTime = itemView.senior_sendtime
}

class SeniorAnswerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var seniorQuestion = itemView.senior_question
    var seniorAnswer = itemView.senior_answer
    var seniorAnswerSendTime = itemView.senior_answer_sendtime
}
typealias SelectorClickedListener = (String, String?, String?) -> Unit
package com.opusone.leanon.messenger

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crashlytics.android.Crashlytics
import com.opusone.leanon.messenger.message.ItemAdapter
import com.opusone.leanon.messenger.message.MessagingManager
import com.opusone.leanon.messenger.message.OoMessageType
import com.opusone.leanon.messenger.util.OoPreference
import com.opusone.leanon.oocontentresolver.OoContentResolver
import com.opusone.leanon.oocontentresolver.model.User
import com.opusone.leanon.oordbobserver.OoRDBObserver
import com.opusone.leanon.oordbobserver.extension.addChatObserver
import com.opusone.leanon.oordbobserver.extension.removeChatObserver
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorestmanager.model.OoChat
import com.opusone.leanon.oorestmanager.restful.*
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.longToast
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.yesButton


class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"

    private var isLastVisible : Boolean = false
    private lateinit var itemAdapter: ItemAdapter

    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initManagers()
        addNetwrokChangedObserver()

        if (!isValidateUser()) {
            MessagingManager.clearRealm()
        }

        initMessageData()
    }

    override fun onResume() {
        super.onResume()

        MessagingManager.updateAppState(this, contentResolver, true)

        if (!isValidateUser()) {
            showInvalidUSerDialog()
        }
    }

    override fun onPause() {
        super.onPause()

        MessagingManager.updateAppState(this, contentResolver, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        dettachGroupChatObserver()
    }

    private fun initManagers() {
        Fabric.with(this, Crashlytics())

        OoNetworkManager.init(this)

        OoRestManager.init(true)
        OoRestManager.setReachable(OoNetworkManager) {
            applicationContext.runOnUiThread {
                longToast(R.string.toast_lost_network)
            }
        }

        OoRealmManager.initRealm(this)
    }

    private fun addNetwrokChangedObserver() {
        OoNetworkManager.onNetworkChangeListener = { status ->
            when (status) {
                OoNetworkStatus.LOST -> {
                    runOnUiThread {
                        spinner.visibility = View.INVISIBLE
//                        longToast(R.string.toast_lost_network)
                    }
                }

                OoNetworkStatus.CELLULAR -> {
//                    runOnUiThread {
//                        longToast(R.string.toast_available_mobile_network)
//                    }
                }

                OoNetworkStatus.WiFi -> {
//                    runOnUiThread {
//                        longToast(R.string.toast_available_wifi_network)
//                    }
                }
            }
        }
    }

    private fun initMessageData() {
        spinner.visibility = View.VISIBLE

        MessagingManager.initData(user)
        initView()
        attachGroupChatObserver()
    }

    private fun isValidateUser(): Boolean {
        var ret = true
        try {
            user = OoContentResolver.getUserInfo(contentResolver)

            if (user == null || user?.id == null || (user?.id ?: "").isEmpty()) {
                ret = false
            }

            val preferenceUserId = OoPreference.getUserID(this) ?: ""
            if (!preferenceUserId.isEmpty() && (user?.id != preferenceUserId)) {
                ret = false
            }

            OoPreference.setUserID(this, user?.id ?: "")

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            ret = false
        }
        return ret
    }

    private fun showInvalidUSerDialog() {
        val context = this
        alert ("린온 앱내에서 실행해주세요."){
            yesButton {
                MessagingManager.clearRealm()
                OoPreference.setUserID(context, "")
                finish()
            }
        }.show().setCancelable(false)
    }

    private fun attachGroupChatObserver() {
        Log.i(TAG, "attachObserver")

        val userId = user?.id
        if (userId == null || userId.isEmpty()) {
            messageview.adapter?.notifyDataSetChanged()
            spinner.visibility = View.INVISIBLE
            return
        }

        if ((MessagingManager.dataSet?.size ?: 0) > 0) {
            MessagingManager.dataSet?.last()?.timestamp?.let {
                OoRDBObserver.addChatObserver(userId, it, ::onChatAdded)
            }
            spinner.visibility = View.INVISIBLE
            return
        }

        OoRestManager.getRecentGroupChatList(userId, 0) { error, response ->
            if (error != null) {
                OoRDBObserver.addChatObserver(userId, 0, ::onChatAdded)
                spinner.visibility = View.INVISIBLE
                return@getRecentGroupChatList
            }

            response?.chatList?.let {
                it.forEach { chat -> MessagingManager.addChat(chat)}
                MessagingManager.checkFirstData()
                scrollToBottom(true)

                if ((MessagingManager.dataSet?.size ?: 0) > 0) {
                    MessagingManager.dataSet?.last()?.timestamp.let {
                        OoRDBObserver.addChatObserver(userId, it ?: 0, ::onChatAdded)
                    }
                } else {
                    OoRDBObserver.addChatObserver(userId, 0, ::onChatAdded)
                }
            }
            spinner.visibility = View.INVISIBLE
        }
    }

    private fun dettachGroupChatObserver() {
        MessagingManager.closeRealm()
        OoRDBObserver.removeChatObserver()
    }

    private fun onChatAdded(chat: OoChat?) {
        Log.d(TAG, "onChatAdded: $chat")
        if (chat?.id.equals(user?.id) && chat?.deviceType.equals(OoLeanonDeviceType.SENIOR_TABLET.type)) {
            return
        }
        MessagingManager.addChat(chat)

        messageview.adapter?.itemCount?.let {
            if(isLastVisible && it > 0) {
                scrollToBottom(false)

            } else {
                gotoNewMessage.visibility = View.VISIBLE
                gotoNewMessage.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.blink_anim))
            }
        }
    }

    private fun initView() {
        initSendButton()

        user?.name?.let {
            if (!it.isEmpty()) {
                chatroom_title.setText("${it}의 채팅방")
            }
        }

        MessagingManager.dataSet?.let {
            itemAdapter = ItemAdapter(user, it)

            itemAdapter.selectorClickedListener = selectorClickedListener@{ selected, tag, message->
                MessagingManager.sendMessage(applicationContext, message, OoMessageType.ITEM_MESSAGE_SELECTED, selected, tag)
                scrollToBottom(true)
            }

            messageview.adapter = itemAdapter
            messageview.addOnScrollListener(onScrollListener)
            scrollToBottom(false)

            gotoNewMessage.setOnClickListener { view ->
                scrollToBottom(true)
                view.visibility = View.GONE
                view.clearAnimation()
            }
        }
    }

    private fun scrollToBottom(smoothScroll : Boolean) {
        MessagingManager.dataSet?.let {
            runOnUiThread {
                if (it.size > 0) {
                    if (smoothScroll) {
                        messageview.smoothScrollToPosition(it.size -1)
                    } else {
                        messageview.scrollToPosition(it.size -1)
                    }
                }
            }
        }
    }

    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val layoutManager = LinearLayoutManager::class.java.cast(recyclerView.layoutManager)
            val totalItemCount = layoutManager!!.itemCount
            val lastVisible = layoutManager.findLastCompletelyVisibleItemPosition()

            isLastVisible = lastVisible >= totalItemCount - 1
            if(isLastVisible) {
                gotoNewMessage.visibility = View.GONE
                gotoNewMessage.clearAnimation()
            }
        }
    }

    private fun initSendButton() {
        sendMessageBtn.isEnabled = false
        messageEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                sendMessageBtn.isEnabled = !messageEditText.text.toString().equals("")
            }
        })
        sendMessageBtn.setOnClickListener(::sendMessageBtnClickListener)
    }

    private fun sendMessageBtnClickListener(view : View) {
        messageEditText.text?.toString()?.let {
            MessagingManager.sendMessage(applicationContext, it, OoMessageType.ITEM_MESSAGE_NORMAL, null, null)
            messageEditText.setText("")
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(messageEditText.windowToken, 0)
            messageview.adapter?.itemCount?.let {
                messageview.scrollToPosition(it-1)
            }
        }
    }
}

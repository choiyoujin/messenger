package com.opusone.leanon.messenger.message

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.opusone.leanon.messenger.R
import io.realm.RealmList
import kotlinx.android.synthetic.main.item_selector_btn.view.*

class SelectorAdapter(private var selectorList: RealmList<String?>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    private val TAG = "SelectorAdapter"

    var onSelectorClicked: OnSelectorClicked? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SelectorViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_selector_btn, parent, false))
    }

    override fun getItemCount(): Int {
        return selectorList?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val selectedHolder = holder as? SelectorViewHolder
        selectedHolder?.let {
            it.selector.text = selectorList?.get(position)
            it.selector.setOnClickListener{
                onSelectorClicked?.let {
                    it(selectorList?.get(position))
                }
            }
        }
    }

    inner class SelectorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var selector = itemView.selectorBtn
    }
}

typealias OnSelectorClicked = (String?) -> Unit
package com.opusone.leanon.messenger.message

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import com.opusone.leanon.messenger.R
import com.opusone.leanon.messenger.util.OoPreference
import com.opusone.leanon.oocontentresolver.OoContentResolver
import com.opusone.leanon.oocontentresolver.model.User
import com.opusone.leanon.oordbobserver.OoRDBObserver
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorealmmanager.model.OoRmMessage
import com.opusone.leanon.oorealmmanager.model.OoRmUser
import com.opusone.leanon.oorestmanager.model.OoChat
import com.opusone.leanon.oorestmanager.params.OoParamChat
import com.opusone.leanon.oorestmanager.restful.OoLeanOnDevice
import com.opusone.leanon.oorestmanager.restful.OoNetworkManager
import com.opusone.leanon.oorestmanager.restful.OoRestManager
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.runOnUiThread
import java.text.SimpleDateFormat
import java.util.*

object MessagingManager {
    private const val TAG = "MessagingManager"

    private const val AUTHORITY  = "com.opusone.leanon.oorealmprovider"
    private const val MESSAGE_STATE_PATH = "messageState"
    private val MESSAGE_STATE_URI = Uri.parse("content://$AUTHORITY/$MESSAGE_STATE_PATH")

    private var messageRealmInfo: Pair<Realm, RealmResults<OoRmMessage>>? = null

    private var user: User? = null

    var dataSet: RealmResults<OoRmMessage>? = null
        private set
        get() {
            if (messageRealmInfo == null) {
                return null
            }
            return messageRealmInfo?.second
        }

    fun initData(user: User?) {
        this.user = user
        user?.id?.let {
            messageRealmInfo = OoRealmManager.getLauncherGroupChat(it)
        }
    }

    fun closeRealm() {
        messageRealmInfo?.first?.close()
        messageRealmInfo = null
    }

    fun clearRealm() {
        closeRealm()
        OoRealmManager.clear()
    }

    fun checkFirstData() {
        if ((dataSet?.size ?: 0) <= 0) {
            return
        }

        dataSet?.first()?.let {
            if (it.dataType != OoMessageType.ITEM_MESSAGE_DATECHANGED) {
                it.timestamp?.let {
                    val date = OoRmMessage()
                    date.dataType = OoMessageType.ITEM_MESSAGE_DATECHANGED
                    date.timestamp = it - 1
                    OoRealmManager.create(date)
                }
            }
        }
    }

    fun addChat(chat : OoChat?) {
        addViewOnDateChanged(chat?.timestamp) {
        }

        val messageData = OoRmMessage()
        messageData.id = chat?.id
        messageData.picture = chat?.picture
        messageData.name = chat?.name
        messageData.messageAnswer?.addAll(chat?.answer ?: arrayListOf())
        messageData.message = chat?.message
        messageData.question = chat?.timestamp.toString()
        messageData.dataType = chat?.type?.toInt()
        messageData.timestamp = chat?.timestamp
        messageData.question = chat?.timestamp.toString()
        OoRealmManager.create(messageData)
    }

    fun isDateDiff(a : Long?, b : Long?) : Boolean {
        val dateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
        return dateFormat.format(a).toInt() > dateFormat.format(b).toInt()
    }

    fun addViewOnDateChanged(presentTime : Long?, completion: (Long?) -> Unit) {
        presentTime?.let { presentTime ->
            val date = OoRmMessage()
            date.dataType = OoMessageType.ITEM_MESSAGE_DATECHANGED
            date.timestamp = presentTime - 1
            if(OoRealmManager.getMessageCount().toInt().equals(0)) {
                OoRealmManager.create(date)
                completion(date.index)

            } else {
                OoRealmManager.findMessageByIndex(OoRealmManager.getMessageCount()) {
                    if(isDateDiff(
                            presentTime,
                            it?.timestamp
                        )
                    ) {
                        OoRealmManager.create(date)
                        completion(date.index)
                    } else {
                        completion(null)
                    }
                }
            }
        }
    }

    fun updateAppState(context: Context, contentResolver: ContentResolver, state : Boolean) {
        try {
            OoPreference.getUserID(context)?.let {
                if (!it.isEmpty()) {
                    val value = ContentValues()
                    contentResolver.update(MESSAGE_STATE_URI, value, state.toString(), null)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun sendMessage(context: Context, message: String?, dataType : Int, selected: String?, tag: String?) {
        if (!OoNetworkManager.isReachable()) {
            context.runOnUiThread {
                longToast(R.string.toast_lost_network)
            }
            return
        }
        val timestamp = Calendar.getInstance().timeInMillis
        var dateViewIndex : Long? = null
        addViewOnDateChanged(timestamp) {
            it?.let {
                dateViewIndex = it
            }
        }
        val param : OoParamChat
        if (selected != null) {
            param = OoParamChat(user?.userToken, user?.id,"$dataType",null, mutableListOf(selected), tag)
        } else {
            param = OoParamChat(user?.userToken, user?.id,"$dataType", message)
        }
        val messageData = OoRmMessage()
        messageData.message = message
        messageData.dataType = dataType
        messageData.timestamp = timestamp
        messageData.id = user?.id
        messageData.messageAnswer = RealmList(selected)
        messageData.name = user?.name
        OoRealmManager.create(messageData)
        OoRestManager.sendGroupChat(param) { error, response ->
            Log.i(TAG, "error is $error, response is $response")
            response?.chat?.timestamp?.let {ts ->
                dateViewIndex?.let {
                    OoRealmManager.updateByIndex(it) {
                        it.timestamp = ts - 1
                    }
                }
                OoRealmManager.updateByIndex(messageData.index) {
                    it.timestamp = ts
                }
                tag?.let {
                    OoRealmManager.updateByTag(it) {
                        it.dataType = OoMessageType.ITEM_MESSAGE_NORMAL
                    }
                }
            }
                error?.let {
                    context.runOnUiThread {
                        longToast(R.string.response_error)
                    }
                    dateViewIndex?.let {
                        OoRealmManager.deleteMessageByIndex(it)
                    }
                    OoRealmManager.deleteMessageByIndex(messageData.index)
                }
        }
    }
}

class OoMessageType {
    companion object {
        val ITEM_MESSAGE_NORMAL = 0
        val ITEM_MESSAGE_SELECTOR = 1
        val ITEM_MESSAGE_SELECTED = 2
        val ITEM_MESSAGE_NORMAL_SENT = 3
        val ITEM_MESSAGE_SELECTOR_SENT = 4
        val ITEM_MESSAGE_SELECTED_SENT = 5
        val ITEM_MESSAGE_DATECHANGED = 6

    }
}